## CollectionViewSource


The CollectionViewSource is used to filter ListView/TreeView or any kind of view that deals with list of items to be displayed. It has certain important attribute that needs to be implemented for it to work properly. They are source, filter.

Initiate the CollectionViewSource object and then assign the list item of the model view to the source attribute of the CollectionViewSource.
Create a {SearchBoxTypeEventHandler} filter event handler and assign the event handler to the {filter} 
attribute of {CollectionViewSource}, in the event handler we just mention which item is accepted 
and which is not.Bind the {FilterText} to the TextBox. In the set field we implement the RaisePropertyChanged that will trigger the set function when we type in the TextBox. Here we call the event handler. In the event handler it will go through each item, compare them to the text in the textbox and then declare as accepted and not.Based on the acceptance, list/collection that is attributed to the {source} attribute will be refreshed.

The list item needs to be part of the modelview.